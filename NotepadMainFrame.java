package com.sxt.nodepad.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.PrintJob;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.undo.UndoManager;

import com.sxt.nodepad.util.Clock;
import com.sxt.nodepad.util.MQFontChooser;
import com.sxt.nodepad.util.SystemParam;

public class NotepadMainFrame extends JFrame implements ActionListener {

	/**
	 * 序列号
	 */
	private static final long serialVersionUID = 8585210209467333480L;
	private JPanel contentPane;
	private JTextArea textArea;
	private JMenuItem itemOpen;
	private JMenuItem itemSave;

	// 1：新建
	// 2：修改过
	// 3：保存过的
	int flag = 0;

	// 当前文件名
	String currentFileName = null;

	PrintJob p = null;// 声明一个要打印的对象
	Graphics g = null;// 要打印的对象

	// 当前文件路径
	String currentPath = null;

	// 背景颜色
	JColorChooser jcc1 = null;
	Color color = Color.BLACK;

	// 文本的行数和列数
	int linenum = 1;
	int columnnum = 1;

	// 撤销管理器
	public UndoManager undoMgr = new UndoManager();

	// 剪贴板
	public Clipboard clipboard = new Clipboard("系统剪切板");

	private JMenuItem itemNew;
	private JSeparator separator;
	private JMenuItem itemPrint;
	private JMenuItem itemExit;
	private JSeparator separator_1;
	private JMenu itemEdit;
	private JMenu itFormat;
	private JMenu itemHelp;
	private JMenuItem itemSearchForHelp;

	private JMenuItem itemFind;

	private JMenuItem itemTime;
	private JMenuItem itemView;
	private JMenuItem itemColor;
	private JMenuItem itemViewColor;
	private JCheckBoxMenuItem itemNextLine;
	private JScrollPane scrollPane;
	private JCheckBoxMenuItem itemStatement;
	private JToolBar toolState;
	public static JLabel label1;
	private JLabel label2;
	private JLabel label3;

	private JMenuItem popM_Cut;
	private JMenuItem popM_Copy;
	private JMenuItem popM_Paste;
	private JPopupMenu popupMenu;
	int length = 0;
	int sum = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NotepadMainFrame frame = new NotepadMainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	GregorianCalendar c = new GregorianCalendar();
	int hour = c.get(Calendar.HOUR_OF_DAY);
	int min = c.get(Calendar.MINUTE);
	int second = c.get(Calendar.SECOND);

	/**
	 * Create the frame.
	 */
	public NotepadMainFrame() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
		setTitle("NewFile");
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 521, 572);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu itemFile = new JMenu("File (F)");
		itemFile.setMnemonic('F');
		menuBar.add(itemFile);

		itemNew = new JMenuItem("New (N)", 'N');
		itemNew.setAccelerator(KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_N, java.awt.Event.CTRL_MASK));
		itemNew.addActionListener(this);
		itemFile.add(itemNew);

		itemOpen = new JMenuItem("Open (O)", 'O');
		itemOpen.setAccelerator(KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_O, java.awt.Event.CTRL_MASK));
		itemOpen.addActionListener(this);
		itemFile.add(itemOpen);

		itemSave = new JMenuItem("Save (S)");
		itemSave.setAccelerator(KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_S, java.awt.Event.CTRL_MASK));
		itemSave.addActionListener(this);
		itemFile.add(itemSave);

		separator = new JSeparator();
		itemFile.add(separator);

		itemPrint = new JMenuItem("Print (P)...", 'P');
		itemPrint.setAccelerator(KeyStroke.getKeyStroke(
				java.awt.event.KeyEvent.VK_P, java.awt.Event.CTRL_MASK));
		itemPrint.addActionListener(this);
		itemFile.add(itemPrint);

		separator_1 = new JSeparator();
		itemFile.add(separator_1);

		itemExit = new JMenuItem("Exit (X)", 'X');
		itemExit.addActionListener(this);
		itemFile.add(itemExit);

		itemEdit = new JMenu("Search (E)");
		itemEdit.setMnemonic('E');
		menuBar.add(itemEdit);

		itemFind = new JMenuItem("Search (F)", 'F');
		itemFind.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,
				Event.CTRL_MASK));
		itemFind.addActionListener(this);
		itemEdit.add(itemFind);

		itemTime = new JMenuItem("Date/Time (D)", 'D');
		itemTime.addActionListener(this);
		itemTime.setAccelerator(KeyStroke.getKeyStroke("F5"));
		itemEdit.add(itemTime);

		itFormat = new JMenu("View (O)");
		itFormat.setMnemonic('O');
		menuBar.add(itFormat);

		itemNextLine = new JCheckBoxMenuItem("Auto(W)");// /////
		itemNextLine.addActionListener(this);
		itFormat.add(itemNextLine);

		itemView = new JMenuItem("FrontSize(F)...");
		itemView.addActionListener(this);
		itFormat.add(itemView);

		itemColor = new JMenuItem("BackgroundColour(C)...");
		itemColor.addActionListener(this);
		itFormat.add(itemColor);

		itemViewColor = new JMenuItem("Colour(I)...");
		itemViewColor.addActionListener(this);
		itFormat.add(itemViewColor);

		itemStatement = new JCheckBoxMenuItem("T&P (S)");
		itemStatement.addActionListener(this);
		itFormat.add(itemStatement);

		itemHelp = new JMenu("Help (H)");
		itemHelp.setMnemonic('H');
		menuBar.add(itemHelp);

		itemSearchForHelp = new JMenuItem("About (A)", 'H');
		itemSearchForHelp.addActionListener(this);
		itemHelp.add(itemSearchForHelp);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		// 设置边框布局
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		textArea = new JTextArea();

		// VERTICAL垂直 HORIZONTAL水平
		scrollPane = new JScrollPane(textArea,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		popupMenu = new JPopupMenu();
		addPopup(textArea, popupMenu);

		popM_Cut = new JMenuItem("Cut (T)");
		popM_Cut.addActionListener(this);
		popupMenu.add(popM_Cut);

		popM_Copy = new JMenuItem("Copy(C)");
		popM_Copy.addActionListener(this);
		popupMenu.add(popM_Copy);

		popM_Paste = new JMenuItem("Paste (P)");
		popM_Paste.addActionListener(this);
		popupMenu.add(popM_Paste);

		// 添加到面板中【中间】
		contentPane.add(scrollPane, BorderLayout.CENTER);

		// 添加撤销管理器
		textArea.getDocument().addUndoableEditListener(undoMgr);

		toolState = new JToolBar();
		toolState.setSize(textArea.getSize().width, 10);// toolState.setLayout(new
														// FlowLayout(FlowLayout.LEFT));
		label1 = new JLabel("    CurrentTime：" + hour + ":" + min + ":"
				+ second + " ");
		toolState.add(label1);
		toolState.addSeparator();
		label2 = new JLabel("    No. " + linenum + " Row, No. " + columnnum
				+ " Column ");
		toolState.add(label2);
		toolState.addSeparator();

		label3 = new JLabel("    Total: " + length + " Words  ");
		toolState.add(label3);
		textArea.addCaretListener(new CaretListener() { // 记录行数和列数
			public void caretUpdate(CaretEvent e) {
				// sum=0;
				JTextArea editArea = (JTextArea) e.getSource();

				try {
					int caretpos = editArea.getCaretPosition();
					linenum = editArea.getLineOfOffset(caretpos);
					columnnum = caretpos - textArea.getLineStartOffset(linenum);
					linenum += 1;
					label2.setText("    No. " + linenum + " Row, No. "
							+ columnnum + " Column ");
					// sum+=columnnum+1;
					// length+=sum;
					length = NotepadMainFrame.this.textArea.getText()
							.toString().length();
					label3.setText("    Total: " + length + " Words  ");
				} catch (Exception ex) {
				}
			}
		});

		contentPane.add(toolState, BorderLayout.SOUTH);
		toolState.setVisible(false);
		toolState.setFloatable(false);
		Clock clock = new Clock();
		clock.start();

		// 创建弹出菜单
		final JPopupMenu jp = new JPopupMenu(); // 创建弹出式菜单，下面三项是菜单项
		textArea.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3)// 只响应鼠标右键单击事件
				{
					jp.show(e.getComponent(), e.getX(), e.getY());// 在鼠标位置显示弹出式菜单
				}
			}
		});

		isChanged();

		this.MainFrameWidowListener();
	}

	private void isChanged() {
		textArea.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				// 在这里我进行了对使用快捷键，但是没有输入字符却没有改变textArea中内容的判断
				Character c = e.getKeyChar();
				if (c != null && !textArea.getText().toString().equals("")) {
					flag = 2;
				}
			}
		});
	}

	private void MainFrameWidowListener() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (flag == 2 && currentPath == null) {
					// 这是弹出小窗口
					// 1、（刚启动Notepad为0，刚新建文档为1）条件下修改后
					int result = JOptionPane.showConfirmDialog(
							NotepadMainFrame.this, "Save to NewFile?",
							"Notepad", JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.PLAIN_MESSAGE);
					if (result == JOptionPane.OK_OPTION) {
						NotepadMainFrame.this.saveAs();
					} else if (result == JOptionPane.NO_OPTION) {
						NotepadMainFrame.this.dispose();
						NotepadMainFrame.this
								.setDefaultCloseOperation(EXIT_ON_CLOSE);
					}
				} else if (flag == 2 && currentPath != null) {
					// 这是弹出小窗口
					// 1、（刚启动Notepad为0，刚新建文档为1）条件下修改后
					int result = JOptionPane.showConfirmDialog(
							NotepadMainFrame.this, "Save to" + currentPath
									+ "?", "Notepad",
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.PLAIN_MESSAGE);
					if (result == JOptionPane.OK_OPTION) {
						NotepadMainFrame.this.save();
					} else if (result == JOptionPane.NO_OPTION) {
						NotepadMainFrame.this.dispose();
						NotepadMainFrame.this
								.setDefaultCloseOperation(EXIT_ON_CLOSE);
					}
				} else {
					// 这是弹出小窗口
					int result = JOptionPane.showConfirmDialog(
							NotepadMainFrame.this, "Exit?", "Alert",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.PLAIN_MESSAGE);
					if (result == JOptionPane.OK_OPTION) {
						NotepadMainFrame.this.dispose();
						NotepadMainFrame.this
								.setDefaultCloseOperation(EXIT_ON_CLOSE);
					}
				}
			}
		});
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == itemOpen) { // 打开
			openFile();
		} else if (e.getSource() == itemSave) { // 保存
			// 如果该文件是打开的，就可以直接保存
			save();
		} else if (e.getSource() == itemNew) { // 新建
			newFile();
		} else if (e.getSource() == itemExit) { // 退出
			exit();
		} else if (e.getSource() == itemPrint) { // 打印
			// 打印机
			Print();
		} else if (e.getSource() == itemFind) { // 查找
			mySearch();
		} else if (e.getSource() == itemTime) { // 时间/日期
			textArea.append(hour + ":" + min + " " + c.get(Calendar.YEAR) + "/"
					+ (c.get(Calendar.MONTH) + 1) + "/"
					+ c.get(Calendar.DAY_OF_MONTH));
		} else if (e.getSource() == itemNextLine) { // 设置自动换行
			// 设置文本区的换行策略。如果设置为 true，则当行的长度大于所分配的宽度时，将换行。此属性默认为 false。
			if (itemNextLine.isSelected()) {
				textArea.setLineWrap(true);
			} else {
				textArea.setLineWrap(false);
			}
		} else if (e.getSource() == itemView) { // 设置字体大小
			// 构造字体选择器，参数字体为预设值
			MQFontChooser fontChooser = new MQFontChooser(textArea.getFont());
			fontChooser.showFontDialog(this);
			Font font = fontChooser.getSelectFont();
			// 将字体设置到JTextArea中
			textArea.setFont(font);
		} else if (e.getSource() == itemColor) { // 设置背景颜色
			jcc1 = new JColorChooser();
			JOptionPane.showMessageDialog(this, jcc1, "backgroundColour", -1);
			color = jcc1.getColor();
			textArea.setBackground(color);
		} else if (e.getSource() == itemViewColor) { // 设置字体颜色
			jcc1 = new JColorChooser();
			JOptionPane.showMessageDialog(this, jcc1, "chooseColour" + "", -1);
			color = jcc1.getColor();
			// String string=textArea.getSelectedText();
			textArea.setForeground(color);
		} else if (e.getSource() == itemStatement) { // 设置状态
			if (itemStatement.isSelected()) {
				// scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
				toolState.setVisible(true);
				
				return;
			}
			// scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			toolState.setVisible(false);

		} else if (e.getSource() == itemSearchForHelp) {
			JOptionPane.showMessageDialog(this,
					"ID: 15398019 Name: DENG Jiawei \n ID: 15398124 Name: LI Jiayang", "Team member", 1);
		} else if (e.getSource() == popM_Cut) { // 剪切
			cut();
		} else if (e.getSource() == popM_Copy) { // 复制
			copy();
		} else if (e.getSource() == popM_Paste) { // 粘贴
			paste();
		}
	}

	private void exit() {
		if (flag == 2 && currentPath == null) {
			// 这是弹出小窗口
			// 1、（刚启动Notepad为0，刚新建文档为1）条件下修改后
			int result = JOptionPane
					.showConfirmDialog(NotepadMainFrame.this,
							"Save to NewFile?", "Notepad",
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				NotepadMainFrame.this.saveAs();
			} else if (result == JOptionPane.NO_OPTION) {
				NotepadMainFrame.this.dispose();
				NotepadMainFrame.this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			}
		} else if (flag == 2 && currentPath != null) {
			// 这是弹出小窗口
			// 1、（刚启动Notepad为0，刚新建文档为1）条件下修改后
			int result = JOptionPane
					.showConfirmDialog(NotepadMainFrame.this, "Save to"
							+ currentPath + "?", "Notepad",
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				NotepadMainFrame.this.save();
			} else if (result == JOptionPane.NO_OPTION) {
				NotepadMainFrame.this.dispose();
				NotepadMainFrame.this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			}
		} else {
			// 这是弹出小窗口
			int result = JOptionPane.showConfirmDialog(NotepadMainFrame.this,
					"Exit？", "Alert!", JOptionPane.YES_NO_OPTION,
					JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				NotepadMainFrame.this.dispose();
				NotepadMainFrame.this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			}
		}
	}

	private void newFile() {
		if (flag == 0 || flag == 1) { // 刚启动Notepad为0，刚新建文档为1
			return;
		} else if (flag == 2 && this.currentPath == null) { // 修改后
			// 1、（刚启动Notepad为0，刚新建文档为1）条件下修改后

			this.textArea.setText("");
			this.setTitle("NewFile");
			flag = 1;

			return;
		} else if (flag == 2 && this.currentPath != null) {
			// 2、（保存的文件为3）条件下修改后
			int result = JOptionPane
					.showConfirmDialog(NotepadMainFrame.this, "Save to"
							+ this.currentPath + "?", "Notepad",
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				this.save(); // 直接保存，有路径
			} else if (result == JOptionPane.NO_OPTION) {
				this.textArea.setText("");
				this.setTitle("NewFile");
				flag = 1;
			}
		} else if (flag == 3) { // 保存的文件
			this.textArea.setText("");
			flag = 1;
			this.setTitle("NewFile");
		}
	}

	private void saveAs() {
		// 打开保存框
		JFileChooser choose = new JFileChooser();
		// 选择文件
		int result = choose.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			// 取得选择的文件[文件名是自己输入的]
			File file = choose.getSelectedFile();
			FileWriter fw = null;
			// 保存
			try {
				fw = new FileWriter(file);
				fw.write(textArea.getText());
				currentFileName = file.getName();
				currentPath = file.getAbsolutePath();
				// 如果比较少，需要写
				fw.flush();
				this.flag = 3;
				this.setTitle(currentPath);
			} catch (IOException e1) {
				e1.printStackTrace();
			} finally {
				try {
					if (fw != null)
						fw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	private void save() {
		if (this.currentPath == null) {
			this.saveAs();
			if (this.currentPath == null) {
				return;
			}
		}
		FileWriter fw = null;
		// 保存
		try {
			fw = new FileWriter(new File(currentPath));
			fw.write(textArea.getText());
			// 如果比较少，需要写
			fw.flush();
			flag = 3;
			this.setTitle(this.currentPath);
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			try {
				if (fw != null)
					fw.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void openFile() {
		if (flag == 2 && this.currentPath == null) {
			// 1、（刚启动Notepad为0，刚新建文档为1）条件下修改后
			int result = JOptionPane
					.showConfirmDialog(NotepadMainFrame.this,
							"Save to NewFile?", "Notepad",
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				this.saveAs();
			}
		} else if (flag == 2 && this.currentPath != null) {
			// 2、（打开的文件2，保存的文件3）条件下修改
			int result = JOptionPane
					.showConfirmDialog(NotepadMainFrame.this, "Save to "
							+ this.currentPath + "?", "Notepad",
							JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.PLAIN_MESSAGE);
			if (result == JOptionPane.OK_OPTION) {
				this.save();
			}
		}
		// 打开文件选择框
		JFileChooser choose = new JFileChooser();
		// 选择文件
		int result = choose.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			// 取得选择的文件
			File file = choose.getSelectedFile();
			// 打开已存在的文件，提前将文件名存起来
			currentFileName = file.getName();
			// 存在文件全路径
			currentPath = file.getAbsolutePath();
			flag = 3;
			this.setTitle(this.currentPath);
			BufferedReader br = null;
			try {
				// 建立文件流[字符流]
				InputStreamReader isr = new InputStreamReader(
						new FileInputStream(file), "GBK");
				br = new BufferedReader(isr);// 动态绑定
				// 读取内容
				StringBuffer sb = new StringBuffer();
				String line = null;
				while ((line = br.readLine()) != null) {
					sb.append(line + SystemParam.LINE_SEPARATOR);
				}
				// 显示在文本框[多框]
				textArea.setText(sb.toString());
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} finally {
				try {
					if (br != null)
						br.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public void Print() {
		try {
			p = getToolkit().getPrintJob(this, "ok", null);// 创建一个Printfjob 对象 p
			g = p.getGraphics();// p 获取一个用于打印的 Graphics 的对象
			// g.translate(120,200);//改变组建的位置
			this.textArea.printAll(g);
			p.end();// 释放对象 g
		} catch (Exception a) {

		}
	}

	public void cut() {
		copy();
		// 标记开始位置
		int start = this.textArea.getSelectionStart();
		// 标记结束位置
		int end = this.textArea.getSelectionEnd();
		// 删除所选段
		this.textArea.replaceRange("", start, end);

	}

	public void copy() {
		// 拖动选取文本
		String temp = this.textArea.getSelectedText();
		// 把获取的内容复制到连续字符器，这个类继承了剪贴板接口
		StringSelection text = new StringSelection(temp);
		// 把内容放在剪贴板
		this.clipboard.setContents(text, null);
	}

	public void paste() {
		// Transferable接口，把剪贴板的内容转换成数据
		Transferable contents = this.clipboard.getContents(this);
		// DataFalvor类判断是否能把剪贴板的内容转换成所需数据类型
		DataFlavor flavor = DataFlavor.stringFlavor;
		// 如果可以转换
		if (contents.isDataFlavorSupported(flavor)) {
			String str;
			try {// 开始转换
				str = (String) contents.getTransferData(flavor);
				// 如果要粘贴时，鼠标已经选中了一些字符
				if (this.textArea.getSelectedText() != null) {
					// 定位被选中字符的开始位置
					int start = this.textArea.getSelectionStart();
					// 定位被选中字符的末尾位置
					int end = this.textArea.getSelectionEnd();
					// 把粘贴的内容替换成被选中的内容
					this.textArea.replaceRange(str, start, end);
				} else {
					// 获取鼠标所在TextArea的位置
					int mouse = this.textArea.getCaretPosition();
					// 在鼠标所在的位置粘贴内容
					this.textArea.insert(str, mouse);
				}
			} catch (UnsupportedFlavorException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
	}

	public void mySearch() {
		final JDialog findDialog = new JDialog(this, "Find/Replace", true);

		Container con = findDialog.getContentPane();

		con.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel searchContentLabel = new JLabel("Find(N) :");
		JLabel replaceContentLabel = new JLabel("ReplaceWith(P)　 :");
		final JTextField findText = new JTextField(22);
		final JTextField replaceText = new JTextField(22);
		final JCheckBox matchcase = new JCheckBox("CaseSensitive");
		ButtonGroup bGroup = new ButtonGroup();
		final JRadioButton up = new JRadioButton("Up(U)");
		final JRadioButton down = new JRadioButton("Down(D)");
		down.setSelected(true);
		bGroup.add(up);
		bGroup.add(down);
		JButton searchNext = new JButton("FindNext(F)");
		JButton replace = new JButton("Replace(R)");
		final JButton replaceAll = new JButton("ReplaceALL(A)");
		searchNext.setPreferredSize(new Dimension(110, 22));
		replace.setPreferredSize(new Dimension(110, 22));
		replaceAll.setPreferredSize(new Dimension(110, 22));
		// "替换"按钮的事件处理
		replace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (replaceText.getText().length() == 0
						&& textArea.getSelectedText() != null)
					textArea.replaceSelection("");
				if (replaceText.getText().length() > 0
						&& textArea.getSelectedText() != null)
					textArea.replaceSelection(replaceText.getText());
			}
		});

		// "替换全部"按钮的事件处理
		replaceAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				textArea.setCaretPosition(0); // 将光标放到编辑区开头
				int a = 0, b = 0, replaceCount = 0;

				if (findText.getText().length() == 0) {
					JOptionPane.showMessageDialog(findDialog, "Please Input!",
							"Alert", JOptionPane.WARNING_MESSAGE);
					findText.requestFocus(true);
					return;
				}
				while (a > -1) {

					int FindStartPos = textArea.getCaretPosition();
					String str1, str2, str3, str4, strA, strB;
					str1 = textArea.getText();
					str2 = str1.toLowerCase();
					str3 = findText.getText();
					str4 = str3.toLowerCase();

					if (matchcase.isSelected()) {
						strA = str1;
						strB = str3;
					} else {
						strA = str2;
						strB = str4;
					}

					if (up.isSelected()) {
						if (textArea.getSelectedText() == null) {
							a = strA.lastIndexOf(strB, FindStartPos - 1);
						} else {
							a = strA.lastIndexOf(strB, FindStartPos
									- findText.getText().length() - 1);
						}
					} else if (down.isSelected()) {
						if (textArea.getSelectedText() == null) {
							a = strA.indexOf(strB, FindStartPos);
						} else {
							a = strA.indexOf(strB, FindStartPos
									- findText.getText().length() + 1);
						}

					}

					if (a > -1) {
						if (up.isSelected()) {
							textArea.setCaretPosition(a);
							b = findText.getText().length();
							textArea.select(a, a + b);
						} else if (down.isSelected()) {
							textArea.setCaretPosition(a);
							b = findText.getText().length();
							textArea.select(a, a + b);
						}
					} else {
						if (replaceCount == 0) {
							JOptionPane.showMessageDialog(findDialog,
									"Cannot find!", "Notepad",
									JOptionPane.INFORMATION_MESSAGE);
						} else {
							JOptionPane.showMessageDialog(findDialog,
									"Successfully Replaced: " + replaceCount
											+ "changed!", "Replace",
									JOptionPane.INFORMATION_MESSAGE);
						}
					}
					if (replaceText.getText().length() == 0
							&& textArea.getSelectedText() != null) {
						textArea.replaceSelection("");
						replaceCount++;
					}
					if (replaceText.getText().length() > 0
							&& textArea.getSelectedText() != null) {
						textArea.replaceSelection(replaceText.getText());
						replaceCount++;
					}
				}// end while
			}
		}); /* "替换全部"按钮的事件处理结束 */

		// "查找下一个"按钮事件处理
		searchNext.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int a = 0, b = 0;
				int FindStartPos = textArea.getCaretPosition();
				String str1, str2, str3, str4, strA, strB;
				str1 = textArea.getText();
				str2 = str1.toLowerCase();
				str3 = findText.getText();
				str4 = str3.toLowerCase();
				// "区分大小写"的CheckBox被选中
				if (matchcase.isSelected()) {
					strA = str1;
					strB = str3;
				} else {
					strA = str2;
					strB = str4;
				}

				if (up.isSelected()) {
					if (textArea.getSelectedText() == null) {
						a = strA.lastIndexOf(strB, FindStartPos - 1);
					} else {
						a = strA.lastIndexOf(strB, FindStartPos
								- findText.getText().length() - 1);
					}
				} else if (down.isSelected()) {
					if (textArea.getSelectedText() == null) {
						a = strA.indexOf(strB, FindStartPos);
					} else {
						a = strA.indexOf(strB, FindStartPos
								- findText.getText().length() + 1);
					}

				}
				if (a > -1) {
					if (up.isSelected()) {
						textArea.setCaretPosition(a);
						b = findText.getText().length();
						textArea.select(a, a + b);
					} else if (down.isSelected()) {
						textArea.setCaretPosition(a);
						b = findText.getText().length();
						textArea.select(a, a + b);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Cannot find!",
							"Notepad", JOptionPane.INFORMATION_MESSAGE);
				}

			}
		});/* "查找下一个"按钮事件处理结束 */
		// "取消"按钮及事件处理
		JButton cancel = new JButton("cancel");
		cancel.setPreferredSize(new Dimension(110, 22));
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				findDialog.dispose();
			}
		});

		// 创建"查找与替换"对话框的界面
		JPanel bottomPanel = new JPanel();
		JPanel centerPanel = new JPanel();
		centerPanel.setSize(500, 500);
		JPanel topPanel = new JPanel();

		JPanel direction = new JPanel();
		direction.setBorder(BorderFactory.createTitledBorder("Direction "));
		direction.add(up);
		direction.add(down);
		direction.setPreferredSize(new Dimension(170, 60));
		JPanel replacePanel = new JPanel();
		replacePanel.setLayout(new GridLayout(2, 1));
		replacePanel.add(replace);
		replacePanel.add(replaceAll);

		topPanel.add(searchContentLabel);
		topPanel.add(findText);
		topPanel.add(searchNext);
		centerPanel.add(replaceContentLabel);
		centerPanel.add(replaceText);
		centerPanel.add(replacePanel);
		bottomPanel.add(matchcase);
		bottomPanel.add(direction);
		bottomPanel.add(cancel);

		con.add(topPanel);
		con.add(centerPanel);
		con.add(bottomPanel);

		// 设置"查找与替换"对话框的大小、可更改大小(否)、位置和可见性
		findDialog.setSize(500, 210);
		findDialog.setResizable(false);
		findDialog.setLocation(230, 280);
		findDialog.setVisible(true);
	}

}